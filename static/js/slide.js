const _C = document.querySelector('.slide'), 
      N = _C.children.length;

_C.style.setProperty('--n', N)

_C.addEventListener('mousedown', lock, false);
_C.addEventListener('touchstart', lock, false);

_C.addEventListener('mouseup', move, false);
_C.addEventListener('touchend', move, false);

function unify(e) { return e.changedTouches ? e.changedTouches[0] : e };

let x0 = null;



let i = 0;


_C.addEventListener('touchmove', e => {e.preventDefault()}, false)


_C.addEventListener('mousemove', drag, false);
_C.addEventListener('touchmove', drag, false);

let locked = false;

function lock(e) {
    x0 = unify(e).clientX;
    locked = true
  };

  function drag(e) {
    e.preventDefault();
      
    if(locked) {
      let dx = unify(e).clientX - x0, 
        f = +(dx/w).toFixed(2);
          
      _C.style.setProperty('--i', i - f)
    }
  };


let w;

function size() { w = window.innerWidth };


let ini, fin;

function move(e) {
  if(locked) {
    let dx = unify(e).clientX - x0, 
        s = Math.sign(dx), 
        f = +(s*dx/w).toFixed(2);
		
    ini = i - s*f;

    if((i > 0 || s < 0) && (i < N - 1 || s > 0) && f > .2) {
      i -= s;
      f = 1 - f
    }

    fin = i;
    anf = Math.round(f*NF);
    ani();
    x0 = null;
    locked = false;
  }
};

size();

addEventListener('resize', size, false);

const NF = 30;

let rID = null;

function stopAni() {
  cancelAnimationFrame(rID);
  rID = null
};

function ani(cf = 0) {
    _C.style.setProperty('--i', ini + (fin - ini)*cf/anf);
	
    if(cf === anf) {
        stopAni();
        return
    }
	
    rID = requestAnimationFrame(ani.bind(this, ++cf))
};